#!/bin/bash

# a lot of initializing
CACHE_DIRECTORY=$(pwd -P)
ROOT_DIRECTORY="$(dirname "$(readlink -f "$0")")/.."
SCRIPTS="${ROOT_DIRECTORY}/templates/scripts"
SRC="${ROOT_DIRECTORY}/templates/src"
CMAKE="${ROOT_DIRECTORY}/templates/cmake"
CONFIG="${ROOT_DIRECTORY}/config"
TEMPLATES="${ROOT_DIRECTORY}/templates"
PARAMS=()

# parse inputs
for arg in "$@"
do
  case $arg in
    -h|--help)
      echo "$package - initialize cpp project with cmake"
      echo " "
      echo "$package [options] application [arguments]"
      echo " "
      echo "options: "
      echo "-h, --help              show this help dialog"
      echo "-m, --module=MODS       initialize project with module(s)"
      echo "-t, --testing_enabled   initialize project with gtesting"
      echo "-s, --short_name=NAME   initialize project with shortened name for directory names"
      exit 0
      ;;
    -s)
      TITLE_SHORT="${2^^}"
      shift
      shift
      ;;
    --shortname=*)
      TITLE_SHORT="${arg#*=}" 
      TITLE_SHORT="${TITLE_SHORT^^}"
      shift
      ;;
    -t|--testing_enabled)
      INCLUDE_TESTING=1
      shift
      ;;
    -d)
      INCLUDE_DOCUMENTATION=1
      shift
      ;;
    -g)
      INCLUDE_GIT=1
      shift
      ;;
    *)
      PARAMS+=("$1")
      shift
      ;;
  esac
done

# parse required parameters
TITLE="${PARAMS[0]^^}"
HEADER="${PARAMS[0]^}RUN"
MODULES=()
for i in "${!PARAMS[@]}"
do
  [[ $i > 0 ]] && MODULES+=("${PARAMS[$i]^}")
done

# initialization messages
if [ ${#MODULES[@]} -eq 0 ]; then
  echo "Initializing with no modules"
else
  echo "Initializing with modules: ${MODULES[@]}"
fi

# check for input errors
[[ -z $TITLE ]] && echo "project TITLE required as an argument." && exit 0
[[ -z $HEADER ]] && echo "project HEADER required as an argument." && exit 0
# set short title if not set
[[ -z $TITLE_SHORT ]] && TITLE_SHORT=${TITLE^^} && echo "setting TITLE_SHORT"

########################################################

# create working folder
[[ -v $TITLE ]] && rm -r $TITLE
mkdir -p $TITLE
cd $TITLE
mkdir .tmp
touch .locate

[[ $INCLUDE_GIT ]] && git init

# create folder structure, and initialize .gitkeep
xargs -I {} mkdir -p "{}" < "${CONFIG}/filesystem.conf" 
xargs -I {} touch "{}"/.gitkeep < "${CONFIG}/filesystem.conf"

# export variables
[[ -f .config/var.conf ]] && rm .config/var.conf
echo "ROOT_DIRECTORY=${ROOT_DIRECTORY}" >> .config/var.conf
echo "TITLE=${TITLE}" >> .config/var.conf
echo "TITLE_SHORT=${TITLE_SHORT}" >> .config/var.conf
echo "HEADER=${HEADER}" >> .config/var.conf
if [[ $INCLUDE_TESTING ]]; then
  echo "INCLUDE_TESTING=${INCLUDE_TESTING}" >> .config/var.conf
fi
echo "MODULES=(${MODULES[@]})" >> .config/var.conf

# create module folder structure and initialize .gitkeep
if [[ $INCLUDE_TESTING ]]; then  
  mkdir -p test/lib && mkdir -p test/src 
  [[ ! -d test/lib/googletest ]] && \
    git clone "https://github.com/google/googletest.git" "test/lib/googletest"
  cp "${SRC}/main.test.cpp.tmp" "test/main.cpp"
  touch test/src/.gitkeep
  cat "${CMAKE}/test.tmp" \
    | sed -e "s/\${header}/${HEADER}/" \
    | sed -e "s/\${title}/${TITLE}/" \
    > "test/CMakeLists.txt"
fi

# create src structure
cp ${SRC}/main.cpp.tmp ./src/main.cpp
cat "${CMAKE}/src.tmp" \
  | sed -e "s/\${header}/${HEADER}/" \
  | sed -e "s/\${title}/${TITLE}/" \
  > "src/CMakeLists.txt"

# build top CMakeLists.txt
if [[ $INCLUDE_TESTING ]]; then  
sed -e "s/\${project_name}/${TITLE}/" \
  -e "s/\${TITLE}/${TITLE_SHORT}/" \
  -e "s/\${testing}/add_subdirectory(test)/" \
  ${CMAKE}/top.tmp > "CMakeLists.txt"
else
sed -e "s/\${project_name}/${TITLE}/" \
  -e "s/\${TITLE}/${TITLE_SHORT}/" \
  ${CMAKE}/top.tmp > "CMakeLists.txt"
  awk '!/\${testing}/' ./CMakeLists.txt > temp.txt && mv temp.txt ./CMakeLists.txt
fi

# load modules (this comes from the mkmod.sh)
mkmod

# get scripts
cp ${SCRIPTS}/.vimsource .
[[ -d build ]] && \
  rm -r build && \
  mkdir build
cp ${SCRIPTS}/build bin/build.sh
cp ${SCRIPTS}/make bin/make.sh
cp ${SCRIPTS}/docs bin/docs.sh

if [[ $INCLUDE_DOCUMENTATION ]]; then 
  if command -v doxygen >/dev/null; then
    mkdir -p docs && touch docs/.gitkeep
    sed -e "s/\${TITLE}/${TITLE}/" \
      ${TEMPLATES}/Doxyfile > "docs/Doxyfile"
    doxygen docs/Doxyfile
  else
    echo "must install doxygen to use documentation system"
  fi
fi

# return to base directory
cd $CACHE_DIRECTORY
