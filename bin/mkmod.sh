#!/bin/bash

CACHE_DIRECTORY=$(pwd -P)
ROOT_DIRECTORY="$(dirname "$(readlink -f "$0")")/.."
SCRIPTS="${ROOT_DIRECTORY}/templates/scripts"
SRC="${ROOT_DIRECTORY}/templates/src"
CMAKE="${ROOT_DIRECTORY}/templates/cmake"

# get top directory
upsearch () {
  slashes=${CACHE_DIRECTORY//[^\/]/}
  directory="$CACHE_DIRECTORY"
  for (( n=${#slashes}; n>0; --n ))
  do
    [[ -f $directory/$1 ]] && break 
    directory="$directory/.."
  done
  if [ -f $directory/$1 ]; then
    cd $directory
    TOP_DIRECTORY=$(pwd -P)
  else
    echo "Must be in existing mkcpp project" 
    exit 0 
  fi
}
upsearch .locate

# get the other shit
source "${TOP_DIRECTORY}/.config/var.conf" 

# parse inputs
for arg in "$@"
do
  case $arg in
    -h|--help)
      echo "$package - modify project"
      echo " "
      echo "$package [options] application [arguments]"
      echo " "
      echo "options: "
      exit 0
      ;;
    *)
      if [[ ! "${MODULES[@]}" =~ "${1^}" ]]; then 
        MODULES+=("${1^}")
      else
        echo "Cannot create duplicate module"
      fi
      shift
      ;;
  esac
done

# export variables
[[ -f .config/var.conf ]] && rm .config/var.conf
echo "ROOT_DIRECTORY=${ROOT_DIRECTORY}" >> .config/var.conf
echo "TITLE=${TITLE}" >> .config/var.conf
echo "TITLE_SHORT=${TITLE_SHORT}" >> .config/var.conf
echo "HEADER=${HEADER}" >> .config/var.conf
if [[ $INCLUDE_TESTING ]]; then
  echo "INCLUDE_TESTING=${INCLUDE_TESTING}" >> .config/var.conf
fi
echo "MODULES=(${MODULES[@]})" >> .config/var.conf

########################################################

# create modules
for i in "${MODULES[@]}"; do 
  # initialize folder structures 
  if [[ ! -d src/${i} ]]; then
    mkdir -p "src/${i}"
    touch "src/${i}/.gitkeep"
  
    cat "${SRC}/module.cpp.tmp" \
      | sed -e "s/\${module}/${i}.h/" \
      > "src/${i}/${i}.cpp"
    cat "${SRC}/module.h.tmp" \
      | sed -e "s/\${module}/${i^^}/" \
      > "src/${i}/${i}.h"
    cat "${CMAKE}/module.tmp" \
      | sed -e "s/\${module}/${i}/" \
      | sed -e "s/\${title}/${TITLE}/" \
      > "src/${i}/CMakeLists.txt"
  
    # modify top cmake
    sed -i "/\${modules}/a \
      set(${i^^}_HEADERS_DIR \${PROJECT_SOURCE_DIR}/src/${i})" \
      CMakeLists.txt
    sed -i "/\${modules_include}/a \
      include_directories(\${${i^^}_HEADERS_DIR})" \
      CMakeLists.txt
  
    # modify src cmake
    sed -i "/\${module}/a \
      add_subdirectory(${i})" \
      src/CMakeLists.txt
    sed -i "/\${link}/a \
      target_link_libraries(${HEADER} ${i})" \
      src/CMakeLists.txt
  
    # conditional on testing
    if [[ $INCLUDE_TESTING ]]; then
      cat "${SRC}/tests.cpp.tmp" \
        | sed -e "s/\${modules}/${i}.h/" \
        | sed -e "s/\${MODULES}/${i^^}/" \
        > "test/src/${i}.cpp"
  
    # modify test cmake
    sed -i "/\${include}/a \
      include_directories(\${${i^^}_HEADERS_DIR})" \
      test/CMakeLists.txt
    sed -i "/\${link}/a \
      target_link_libraries(${HEADER}_tests ${i} gtest)" \
      test/CMakeLists.txt
    fi
  fi
done

# return to base directory
cd $CACHE_DIRECTORY
